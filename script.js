
// 1.Опишіть, як можна створити новий HTML тег на сторінці.
// За допомогою методу - document.createElement(tag)  
// Наприклад створення тегу div :
// let div = document.createElement('div');


// 2.Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
// tagElement.insertAdjacentHTML(position, text);
//   Параметр  position - визначає позицію елемента що додається, 
//                  щодо елемента, що викликав метод.

//   "beforebegin": до відкриваючого тега.
//   "afterbegin": після відкриваючого тега.
//   "beforeend": перед закриваючим тегом .
//   "afterend": після закриваючого тега.

// 3.Як можна видалити елемент зі сторінки?
// За допомогою методу remove()





// Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку у вигляді списку. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент, до якого буде прикріплений список (по дефолту має бути document.body.
// кожен із елементів масиву вивести на сторінку у вигляді пункту списку;
// Приклади масивів, які можна виводити на екран:

// ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
// ["1", "2", "3", "sea", "user", 23];
// Можна взяти будь-який інший масив.






const listItem = function (arrList, elem = document.body) {
    const ul = document.createElement('ul');
    arrList.forEach((element) => {
        const li = document.createElement('li');
        li.textContent = element;
        ul.append(li);
    });
    elem.prepend(ul);
}

listItem(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);




















